# riff on GKE

Initialize gcloud

```
gcloud init
```

install kubectl

```
gcloud components install kubectl
```

## Project and cluster settings

Create an environment variable for project id, set value to **my-project-name**

```
export GCP_PROJECT_ID=my-project-name
```

Create an enviroment variable for the zone

```
export GCP_ZONE=europe-west4-a
```

Create an environment variable for the gcloud config

```
export GCLOUD_CONFIG=my-project-name-config
```

Create an environment variable for the cluster name

```
export CLUSTER_NAME=my-project-name-cluster-1
```

## Create and set the gcloud configuration

Create a gcloud config

```
gcloud config configurations create $GCLOUD_CONFIG
```

Set the project id

```
gcloud config set project $GCP_PROJECT
```

Set the zone the cluster should be in, e.g. europe-west4-a

```
gcloud config set zone $GCP_ZONE
```

## Prepare the project

Enable the required API's

```
gcloud services enable \
  cloudapis.googleapis.com \
  container.googleapis.com \
  containerregistry.googleapis.com
```

Create the cluster

```
gcloud container clusters create $CLUSTER_NAME \
  --cluster-version=latest \
  --machine-type=n1-standard-2 \
  --enable-autoscaling --min-nodes=1 --max-nodes=3 \
  --enable-autorepair \
  --scopes=service-control,service-management,compute-rw,storage-ro,cloud-platform,logging-write,monitoring-write,pubsub,datastore \
  --num-nodes=3 \
  --zone=$GCP_ZONE
  ```

  Check the kubectl context

  ```
  kubectl config current-context
  ```


  Grant admin rights

  ```
  kubectl create clusterrolebinding cluster-admin-binding \
--clusterrole=cluster-admin \
--user=$(gcloud config get-value core/account)
```