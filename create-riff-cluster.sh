#!/bin/bash

# NOTE: create the project manually, this will enable billing automatically.
# Set these values to your liking
source  ./variables.sh

echo Creating riff cluster with settings:
echo GCP_PROJECT_ID=$GCP_PROJECT_ID
echo GCP_ZONE=$GCP_ZONE
echo GCLOUD_CONFIG=$GCLOUD_CONFIG
echo CLUSTER_NAME=$CLUSTER_NAME

echo Create a gcloud configuration for this project
gcloud config configurations create $GCLOUD_CONFIG
gcloud config set project $GCP_PROJECT_ID

echo Enable the required apis
gcloud services enable \
  cloudapis.googleapis.com \
  container.googleapis.com \
  containerregistry.googleapis.com

echo Create the kubernetes cluster $CLUSTER_NAME
gcloud container clusters create $CLUSTER_NAME \
  --cluster-version=latest \
  --machine-type=n1-standard-2 \
  --enable-autoscaling --min-nodes=1 --max-nodes=3 \
  --enable-autorepair \
  --scopes=service-control,service-management,compute-rw,storage-ro,cloud-platform,logging-write,monitoring-write,pubsub,datastore \
  --num-nodes=3 \
  --zone=$GCP_ZONE

echo Set the cluster admin role
kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user=$(gcloud config get-value core/account)

echo Create push-image service account
gcloud iam service-accounts create push-image  

echo Bind the policy to the sa
gcloud projects add-iam-policy-binding $GCP_PROJECT_ID \
  --member serviceAccount:push-image@$GCP_PROJECT_ID.iam.gserviceaccount.com \
  --role roles/storage.admin

echo Create a secrets directory if it not exists
mkdir -p secrets

# Create keys for the service account
gcloud iam service-accounts keys create \
  --iam-account "push-image@${GCP_PROJECT_ID}.iam.gserviceaccount.com" \
  secrets/${GCP_PROJECT_ID}-gcr-storage-admin.json

echo Install the riff system
riff system install

echo Init the k8s namespace for riff
riff namespace init $RIFF_NAMESPACE --gcr secrets/${GCP_PROJECT_ID}-gcr-storage-admin.json  

