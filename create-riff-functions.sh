#!/bin/bash

# NOTE: create the project manually, this will enable billing automatically.
# Set these values to your liking
source  ./variables.sh

echo Creating riff cluster with these settings:
echo GCP_PROJECT_ID=$GCP_PROJECT_ID
echo GCP_ZONE=$GCP_ZONE
echo GCLOUD_CONFIG=$GCLOUD_CONFIG
echo CLUSTER_NAME=$CLUSTER_NAME
echo ORGANIZATION_ID=$ORGANIZATION_ID

# echo create the riff function square
# riff function create square \
#   --git-repo https://github.com/projectriff-samples/node-square \
#   --artifact square.js \
#   --image $REGISTRY/$REGISTRY_USER/square \
#   --verbose

echo create the riff function echo
riff function create hello \
  --git-repo https://github.com/projectriff-samples/command-hello \
  --artifact greet.sh \
  --image $REGISTRY/$REGISTRY_USER/hello \
  --verbose  

