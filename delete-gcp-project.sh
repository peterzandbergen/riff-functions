#!/bin/bash

# NOTE: create the project manually, this will enable billing automatically.
# Set these values to your liking
source  ./variables.sh

echo Creating riff cluster with these settings:
echo GCP_PROJECT_ID=$GCP_PROJECT_ID
echo GCP_ZONE=$GCP_ZONE
echo GCLOUD_CONFIG=$GCLOUD_CONFIG
echo CLUSTER_NAME=$CLUSTER_NAME
echo ORGANIZATION_ID=$ORGANIZATION_ID

# Create a gcloud configuration for this project
gcloud config configurations create $GCLOUD_CONFIG
gcloud config set project $GCP_PROJECT_ID


# Delete the project
echo Deleting $GCP_PROJECT_ID
gcloud projects delete $GCP_PROJECT_ID

