package main

import (
	"context"
	"fmt"

	"google.golang.org/api/option"
	"google.golang.org/api/cloudbilling/v1"
)


func ProjectSetBillingAccount() error {
	ctx := context.Background()
	as, err := cloudbilling.NewService(ctx, option.WithCredentialsFile("../../secrets"))
	if err != nil {
		return err
	}
	bas := cloudbilling.NewBillingAccountsService(as)
	listCall := bas.List()
	resp, err := listCall.Do()
	if err != nil {
		return err
	}
	_ = resp
	return err
}

func main() {
	err := ProjectSetBillingAccount()
	if err != nil {
		fmt.Printf("error: %s", err.Error())
	}
}