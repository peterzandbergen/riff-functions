package main

import (
	"bytes"
	"fmt"
	"os"
)

type stringsArray []string

func (a stringsArray) String() string {
	var b bytes.Buffer

	for i, s := range a {
		if i == 0 {
			fmt.Fprintf(&b, "%s", s)
		} else {
			fmt.Fprintf(&b, "%s", s)
		}
	}
	return b.String()
}

func main() {
	fmt.Printf("Hello %s\n", stringsArray(os.Args).String())
}
