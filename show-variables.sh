#!/bin/bash

# NOTE: create the project manually, this will enable billing automatically.
# Set these values to your liking
source  ./variables.sh

echo GCP_PROJECT_ID=$GCP_PROJECT_ID
echo GCP_ZONE=$GCP_ZONE
echo GCLOUD_CONFIG=$GCLOUD_CONFIG
echo CLUSTER_NAME=$CLUSTER_NAME
echo ORGANIZATION_ID=$ORGANIZATION_ID
echo REGISTRY=$REGISTRY
echo REGISTRY_USER$REGISTRY_USER