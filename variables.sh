# Settings for the project.

export GCP_PROJECT_ID=cbs-serverless-7
export GCP_ZONE=europe-west4-a
export GCLOUD_CONFIG=cbs-serverless
export CLUSTER_NAME=cbs-serverless-cluster-3
export ORGANIZATION_ID="732357599807"
export RIFF_NAMESPACE=default

export REGISTRY=gcr.io
export REGISTRY_USER=$(gcloud config get-value core/project 2>/dev/null)
